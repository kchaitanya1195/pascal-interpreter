package models

sealed abstract class TokenType(val symbol: String)

object TokenType {
  case object INTEGER extends TokenType("INT")
  case object REAL extends TokenType("REAL")
  case object ID extends TokenType("ID")
  case object PLUS extends TokenType( "+")
  case object MINUS extends TokenType("-")
  case object MULT extends TokenType("*")
  case object DIV extends TokenType("/")
  case object INT_DIV extends TokenType("DIV")
  case object EOF extends TokenType("EOF")
  case object PAREN_OPEN extends TokenType("(")
  case object PAREN_CLOSE extends TokenType(")")
  case object DOT extends TokenType(".")
  case object SEMI extends TokenType(";")
  case object ASSIGN extends TokenType(":=")
  case object COLON extends TokenType(":")
  case object COMMA extends TokenType(",")
  case object CURLY_OPEN extends TokenType("{")
  case object CURLY_CLOSE extends TokenType("}")

  case object PROGRAM extends TokenType("PROGRAM")
  case object PROCEDURE extends TokenType("PROCEDURE")
  case object BEGIN extends TokenType("BEGIN")
  case object END extends TokenType("END")
  case object INT_TYPE extends TokenType("INTEGER")
  case object REAL_TYPE extends TokenType("REAL")
  case object VAR extends TokenType("VAR")


  val exprTokens: Seq[TokenType] = Seq(PLUS, MINUS)
  val exprSymbols: Seq[String] = exprTokens.map(_.symbol)

  val termTokens: Seq[TokenType] = Seq(MULT, DIV, INT_DIV)
  val termSymbols: Seq[String] = termTokens.map(_.symbol)

  val symbols: Seq[TokenType] = Seq(PLUS, MINUS, MULT, DIV, INT_DIV, PAREN_OPEN, PAREN_CLOSE, DOT, SEMI, COMMA, COLON, CURLY_OPEN, CURLY_CLOSE)
  val withSymbol: Map[String, TokenType] = symbols.map(t => t.symbol -> t).toMap

  val keywords: Seq[TokenType] = Seq(PROGRAM, PROCEDURE, BEGIN, END, INT_DIV, INT_TYPE, REAL_TYPE, VAR)
  val fromKeyword: Map[String, TokenType] = keywords.map(t => t.symbol -> t).toMap
  def getKeyword(word: String): TokenType = fromKeyword.getOrElse(word.toUpperCase, ID)

  val binaryOps: Map[TokenType, (Double, Double) => Double] = Map (
    PLUS -> (_ + _),
    MINUS -> (_ - _),
    MULT -> (_ * _),
    DIV -> (_ / _),
    INT_DIV -> (_.toInt / _.toInt)
  )
  val unaryOps: Map[TokenType, Double => Double] = Map (
    PLUS -> identity,
    MINUS -> (-_)
  )

  given Conversion[Char, String] = _.toString
}
