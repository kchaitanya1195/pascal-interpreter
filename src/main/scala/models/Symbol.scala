package models

sealed abstract class Symbol(val name: String, val sType: Option[Symbol]) {
  override def toString: String = s"Symbol(name: $name, sType: $sType)"
  override def equals(other: Any): Boolean =
    other.isInstanceOf[Symbol]
      && other.asInstanceOf[Symbol].name == name
      && other.asInstanceOf[Symbol].sType == sType
}

object Symbol {
  class BuiltInTypeSymbol(name: String) extends Symbol(name, None)
  object BuiltInTypeSymbol {
    def apply(name: String): BuiltInTypeSymbol = new BuiltInTypeSymbol(name)
  }
  
  class VarSymbol(name: String, sType: Symbol) extends Symbol(name, Some(sType))
  object VarSymbol {
    def apply(name: String, sType: Symbol): VarSymbol = new VarSymbol(name, sType)
  }
}
