package models

import models.Symbol.BuiltInTypeSymbol

import scala.collection.mutable

class SymbolTable private (val symbols: Map[String, Symbol]) {
  def define(symbol: Symbol): SymbolTable =
    new SymbolTable(symbols.updated(symbol.name, symbol))

  def lookup(symbolName: String): Option[Symbol] =
    symbols.get(symbolName)

  override def toString: String = s"Symbols: [${symbols.values.mkString("(", "),(", ")")}]"
}

object SymbolTable {
  def apply(): SymbolTable = {
    val builtInSymbols = Map(
      "INTEGER" -> BuiltInTypeSymbol("INTEGER"),
      "REAL" -> BuiltInTypeSymbol("REAL")
    )
    new SymbolTable(builtInSymbols)
  }
}
