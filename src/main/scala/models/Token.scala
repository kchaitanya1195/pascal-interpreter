package models

case class Token(tType: TokenType, value: String, position: Int)
