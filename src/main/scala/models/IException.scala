package models

import models.AST.{TypeSpec, Variable}

class IException(val message: String) extends Exception(message)

object IException {
  case class InvalidTokenException(token: String, position: Int)
    extends IException(s"Invalid token '$token' at position ${position + 1}")
  case class InvalidSyntaxException(found: Token, expected: Seq[TokenType])
    extends IException(s"Found: ${found.value}(${found.tType}). " +
                      s"Expected: ${expected.map(_.symbol).mkString(",")} at position ${found.position + 1}")
  case class UndefinedVariable(v: Variable) extends IException(s"Undefined variable ${v.value} at position ${v.token.position}")
  case class UndefinedType(t: TypeSpec) extends IException(s"Undefined type ${t.token.value} at position ${t.token.position}")
}
