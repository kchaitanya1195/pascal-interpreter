package models

import models.IException.UndefinedVariable
import models.TokenType._

import scala.collection.mutable

sealed abstract class AST

object AST {
  case class BinOp(left: AST, token: Token, right: AST) extends AST
  case class UnOp(token: Token, right: AST) extends AST
  case class Num(token: Token) extends AST
  case class Compound(children: Seq[AST]) extends AST
  case class Assignment(left: Variable, right: AST) extends AST
  case class Variable(token: Token) extends AST {
    val value: String = token.value
  }
  case class TypeSpec(token: Token) extends AST
  case class VarDeclaration(variable: Variable, typeSpec: TypeSpec) extends AST
  case class Block(declarations: Seq[AST], compoundStatement: Compound) extends AST
  case class Program(name: Variable, block: Block) extends AST
  case class ProcedureDeclaration(name: Variable, block: Block) extends AST
  case class Noop() extends AST
}
