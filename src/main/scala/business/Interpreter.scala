package business

import business.Interpreter.Memory
import models.*
import models.IException.UndefinedVariable
import models.TokenType.*

import java.text.DecimalFormat
import scala.util.Try

case class Interpreter(private val parser: Parser) {
  private val df = new DecimalFormat("#")
  df.setMaximumFractionDigits(20)
  df.setMinimumIntegerDigits(1)

  def interpret: (String, Memory) = {
    val node = parser.parse
    val (result, memory) = Interpreter.visit(node, Map.empty)

    (df.format(result), memory)
  }
}

object Interpreter {
  type Memory = Map[String, Double]

  def apply(text: String): Interpreter = new Interpreter(Parser(Lexer(text)))

  // TODO: Put some design pattern here
  def visit(node: AST, memory: Memory): (Double, Memory) = node match {
    case AST.BinOp(left, token, right) =>
      val (leftRes, leftMem) = visit(left, memory)
      val (rightRes, rightMem) = visit(right, leftMem)

      (binaryOps(token.tType)(leftRes, rightRes), rightMem)

    case AST.UnOp(token, right) =>
      val (rightRes, rightMem) = visit(right, memory)
      (unaryOps(token.tType)(rightRes), rightMem)

    case AST.Num(token) =>
      (token.value.toDouble, memory)

    case AST.Compound(children) =>
      (0, children.foldLeft(memory) { case (mem, child) =>
        visit(child, mem)._2
      })

    case AST.Assignment(left, right) =>
      val (rightRes, rightMem) = visit(right, memory)
      (0, rightMem.updated(left.value.toLowerCase, rightRes))

    case variable@AST.Variable(token) =>
      memory.get(token.value.toLowerCase()) match {
        case Some(res) => (res, memory)
        case None => throw UndefinedVariable(variable)
      }

    case _: AST.TypeSpec => (0, memory)
    case _: AST.VarDeclaration => (0, memory)
    case _: AST.ProcedureDeclaration => (0, memory)

    case AST.Block(declarations, compoundStatement) =>
      val decMem = declarations.foldLeft(memory) { case (mem, child) =>
        visit(child, mem)._2
      }
      visit(compoundStatement, decMem)

    case AST.Program(_, block) =>
      visit(block, memory)

    case AST.Noop() => (0, memory)
  }

  def main(args: Array[String]): Unit = {
    Try(Interpreter("   24 * ( 2 -   11   )  ").interpret)
      .recover { case e: IException =>
        e.printStackTrace()
        e.message
      }.map(println)
      .get
  }
}
