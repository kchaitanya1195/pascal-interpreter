package business

import models.IException.InvalidTokenException
import models.{Token, TokenType}
import models.TokenType.{given, *}

import scala.annotation.tailrec
import scala.collection.mutable

case class Lexer(private val text: String) {
  private var charGenerator: Gen = text.toIndexedSeq.zipWithIndex.iterator.buffered
  private val lastP = text.length
  type Gen = collection.BufferedIterator[(Char, Int)]

  private def parseString(start: Char, position: Int, generator: Gen): Token = {
    @tailrec def trParseString(builder: mutable.StringBuilder): mutable.StringBuilder =
      generator.headOption match {
        case Some((c, _)) if c.isLetterOrDigit =>
          builder.append(c)
          generator.next()
          trParseString(builder)
        case _ => builder
      }

    val stringBuilder = new mutable.StringBuilder(start.toString)
    val string = trParseString(stringBuilder).toString()
    Token(getKeyword(string), string, position)
  }

  private def parseInt(start: Char, position: Int, generator: Gen): Token = {
    @tailrec def trParseInt(builder: mutable.StringBuilder, isReal: Boolean): (mutable.StringBuilder, Boolean) =
      generator.headOption match {
        case Some((c, _)) if c.isDigit || (c == '.' && !isReal) =>
          builder.append(c)
          generator.next()
          trParseInt(builder, isReal = isReal || c == '.')
        case _ => builder -> isReal
      }

    val stringBuilder = new mutable.StringBuilder(start.toString)
    val (result, isReal) = trParseInt(stringBuilder, isReal = false)
    if (isReal)
      Token(REAL, result.toString, position)
    else
      Token(INTEGER, result.toString, position)
  }

  private def skipSpaces(generator: Gen): Unit = {
    @tailrec def trSkipSpaces(): Unit =
      generator.headOption match {
        case Some((c, _)) if c.isWhitespace =>
          generator.next()
          trSkipSpaces()
        case _ =>
      }

    trSkipSpaces()
  }

  private def skipComments(generator: Gen): Unit = {
    @tailrec def trSkipComments(): Unit =
      generator.headOption match {
        case Some((c, _)) if c != '}' =>
          generator.next()
          trSkipComments()
        case _ =>
          if (generator.hasNext)
            generator.next()
      }

    trSkipComments()
  }

  @tailrec final def next(generator: Gen = charGenerator): Token = {
    generator.nextOption() match {
      case Some((c, _)) if c.isWhitespace =>
        skipSpaces(generator)
        next(generator)
      case Some((c, _)) if c == '{' =>
        skipComments(generator)
        next(generator)
      case Some((c, i)) if c.isDigit =>
        parseInt(c, i, generator)
      case Some((c, i)) if c.isLetter || c == '_' =>
        parseString(c, i, generator)
      case Some((c, i)) if c == ':' && generator.headOption.exists(_._1 == '=') =>
        generator.next()
        Token(ASSIGN, ":=", i)
      case Some((c, i)) if withSymbol.contains(c) =>
        Token(withSymbol(c), c, i)
      case Some((c, i)) =>
        throw InvalidTokenException(c, i)
      case None =>
        Token(EOF, "", lastP)
    }
  }

  def head: TokenType = {
    val duplicate = charGenerator.duplicate
    val gen = duplicate._1.buffered
    charGenerator = duplicate._2.buffered
    next(gen).tType
  }
}
