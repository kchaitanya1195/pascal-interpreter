package business

import models.IException.{UndefinedType, UndefinedVariable}
import models.Symbol.VarSymbol
import models.{AST, SymbolTable}

case class SymbolTableBuilder(private val parser: Parser) {
  def build: SymbolTable =
    SymbolTableBuilder.build(parser.parse, SymbolTable())
}

object SymbolTableBuilder {
  def apply(text: String): SymbolTableBuilder = new SymbolTableBuilder(Parser(Lexer(text)))

  def build(node: AST, symbolTable: SymbolTable): SymbolTable = node match {
    case AST.BinOp(left, _, right) =>
      val leftSym = build(left, symbolTable)
      build(right, leftSym)

    case AST.UnOp(_, right) =>
      build(right, symbolTable)

    case AST.Num(_) => symbolTable
    case AST.Compound(children) =>
      children.foldLeft(symbolTable){ case (sym, child) => build(child, sym)}

    case AST.Assignment(left, right) =>
      val leftSym = build(left, symbolTable)
      build(right, leftSym)

    case v@AST.Variable(token) =>
      symbolTable.lookup(token.value) match {
        case Some(_) => symbolTable
        case None => throw UndefinedVariable(v)
      }

    case AST.TypeSpec(_) => symbolTable

    case AST.VarDeclaration(variable, typeSpec) =>
      symbolTable.lookup(typeSpec.token.value) match {
        case Some(typeSymbol) =>
          symbolTable.define(VarSymbol(variable.value, typeSymbol))
        case None =>
          throw UndefinedType(typeSpec)
      }

    case AST.ProcedureDeclaration(_, _) => symbolTable

    case AST.Block(declarations, compoundStatement) =>
      val sym = declarations.foldLeft(symbolTable) { case (cSym, child) => build(child, cSym) }
      build(compoundStatement, sym)

    case AST.Program(_, block) =>
      build(block, symbolTable)

    case AST.Noop() => symbolTable
  }
}
