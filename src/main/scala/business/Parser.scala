package business

import models.AST._
import models.IException._
import models._
import models.TokenType._

import scala.annotation.tailrec

case class Parser(private val lexer: Lexer) {
  private def eat(tType: TokenType*): Token = {
    val currentToken = lexer.next()
    if (tType contains currentToken.tType)
      currentToken
    else
      throw InvalidSyntaxException(currentToken, tType)
  }

  def parentheses: AST = {
    eat(PAREN_OPEN)
    val result = expr
    eat(PAREN_CLOSE)

    result
  }

  def factor: AST = {
    @tailrec def trFactor(opAcc: Seq[Token]): AST = {
      lexer.head match {
        case c if exprTokens contains c =>
          val op = eat(exprTokens:_*)
          trFactor(opAcc :+ op)
        case PAREN_OPEN =>
          opAcc.reverse.foldLeft(parentheses)( (ast, op) => UnOp(op, ast) )
        case ID =>
          opAcc.reverse.foldLeft[AST](variable)( (ast, op) => UnOp(op, ast) )
        case _ =>
          opAcc.reverse.foldLeft[AST](Num(eat(INTEGER, REAL)))( (ast, op) => UnOp(op, ast) )
      }
    }

    trFactor(Seq.empty)
  }

  def term: AST = {
    @tailrec def trTerm(left: AST): AST =
      lexer.head match {
        case c if termTokens contains c =>
          val op = eat(termTokens:_*)
          val right = factor

          trTerm(BinOp(left, op, right))
        case _ => left
      }

    trTerm(factor)
  }

  def expr: AST = {
    @tailrec def trExpr(left: AST): AST =
      lexer.head match {
        case c if exprTokens contains c =>
          val op = eat(exprTokens:_*)
          val right = term

          trExpr(BinOp(left, op, right))
        case _ => left
      }

    trExpr(term)
  }

  def empty: AST = Noop()

  def variable: Variable = Variable(eat(ID))

  def typeSpec: TypeSpec = TypeSpec(eat(INT_TYPE, REAL_TYPE))

  def assignment: Assignment = {
    val left = variable
    eat(ASSIGN)
    val right = expr

    Assignment(left, right)
  }

  def statement: AST = {
    val result = lexer.head match {
      case BEGIN => compoundStatement
      case ID => assignment
      case _ => empty
    }

    result
  }

  def statementList: Seq[AST] = {
    @tailrec def trStatementList(stList: Seq[AST]): Seq[AST] = {
      lexer.head match {
        case SEMI =>
          eat(SEMI)
          trStatementList(stList :+ statement)
        case _ => stList
      }
    }

    trStatementList(Seq(statement))
  }

  def compoundStatement: Compound = {
    eat(BEGIN)
    val result = statementList
    eat(END)

    Compound(result)
  }

  def variableDeclaration: Seq[VarDeclaration] = {
    @tailrec def trVariables(varList: Seq[Variable]): Seq[Variable] = {
      lexer.head match {
        case COMMA =>
          eat(COMMA)
          trVariables(varList :+ variable)
        case _ => varList
      }
    }


    val variables = trVariables(Seq(variable))
    eat(COLON)
    val varTypeSpec = typeSpec

    variables.map(declaredVar => VarDeclaration(declaredVar, varTypeSpec))
  }

  def declarations: Seq[AST] = {
    @tailrec def trDeclarations(varDeclarations: Seq[VarDeclaration]): Seq[VarDeclaration] = {
      lexer.head match
        case ID =>
          val nextVarDeclarations = variableDeclaration
          eat(SEMI)
          trDeclarations(varDeclarations ++ nextVarDeclarations)
        case _ =>
          varDeclarations
    }
    @tailrec def trProcedureDeclarations(declarations: Seq[ProcedureDeclaration]): Seq[ProcedureDeclaration] = {
      lexer.head match
        case PROCEDURE =>
          eat(PROCEDURE)
          val name = variable
          eat(SEMI)
          val result = block
          eat(SEMI)

          trProcedureDeclarations(declarations :+ ProcedureDeclaration(name, result))
        case _ => declarations
    }

    val variables = lexer.head match
      case VAR =>
        eat(VAR)
        val nextVarDeclarations = variableDeclaration
        eat(SEMI)
        val variables = trDeclarations(nextVarDeclarations)

        variables
      case _ =>
        Seq.empty
    val procedures = trProcedureDeclarations(Seq.empty)

    variables ++ procedures
  }

  def block: Block = {
    val declarationNodes = declarations
    val statement = compoundStatement

    Block(declarationNodes, statement)
  }

  def program: AST = {
    eat(PROGRAM)
    val name = variable
    eat(SEMI)
    val result = block
    eat(DOT)

    Program(name, result)
  }

  def parse: AST = {
    val result = program
    eat(EOF)
    result
  }
}
