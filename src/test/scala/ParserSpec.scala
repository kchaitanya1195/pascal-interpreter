import business.{Interpreter, Lexer, Parser}
import models.IException
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.text.DecimalFormat
import scala.io.Source
import scala.util.Try

class ParserSpec extends AnyWordSpec with Matchers{
  private val df = new DecimalFormat("#")
  df.setMaximumFractionDigits(20)
  df.setMinimumIntegerDigits(1)

  private val tests = Seq(
    ("add integers", "3+5", "8"),
    ("add zero", "0+5", "5"),
    ("add integers with more than one digit", "31+5", "36"),
    ("add integers with more than one digit 2", "31+235", "266"),
    ("add integers with numerous digits", "311231+23512231", "23823462"),
    ("add integers with spaces", " 311231   + 23512231     ", "23823462"),
    ("subtract zero", "0-5", "-5"),
    ("subtract integers with spaces", " 311231   - 23512231     ", "-23201000"),
    ("multiply integers", "243*232", "56376"),
    ("multiply integers with spaces", " 243    *  232     ", "56376"),
    ("divide integers", "5443 div 232", "23"),
    ("divide integers with spaces", " 5443   div 232     ", "23"),
    ("process single integer", "  34 ", "34"),
    ("process multiple add/subtract operations", "   24 + 0  -1 -34+100+294", "383"),
    ("process multiple multiply/divide operations", "   24 * 23  div 1 div 34*100*294", "470400"),
    ("process expr with operator precedence", "   24 * 2 -   11", "37"),
    ("process parenthesised expr", "   24 * ( 2 -   11   )  ", "-216"),
    ("process parenthesised expr 2", "   ( 2 -   11   )  ", "-9"),
    ("process nested parenthesis", "   24 * ( 2 -   11 * (2+3)   )  ", "-1272"),
    ("process multiple parenthesised operations", "   24 * ( 2 -   11 * (2+3 - 7)   ) div 3 ", "192"),
    ("process unary operation", "+- -- - + - ++++ 54", "-54"),
    ("process sequential op symbols", "3 +- 54", "-51"),
    ("process real numbers", "5 / 2.5", "2"),
    ("process real division", "3 / 6", "0.5")
  )
  private val syntaxTests = Seq(
    ("input has only operator", "+", "Found: (EOF). Expected: INT,REAL at position 2"),
    ("input doesn't end with integer", "3+", "Found: (EOF). Expected: INT,REAL at position 3"),
    ("input doesn't end with integer 2", "3+ ", "Found: (EOF). Expected: INT,REAL at position 4"),
    ("expr has unmatched parens", " ( 3 + 54", "Found: (EOF). Expected: ) at position 10"),
    ("expr has unmatched parens 2", "  3 + ) 54", "Found: )(PAREN_CLOSE). Expected: INT,REAL at position 7")
  )
  private val programErrorTests = Seq(
    ("no dot at end", Left("PROGRAM dot; BEGIN END"), "Found: (EOF). Expected: . at position 23"),
    ("no begin", Left("PROGRAM _begin; a := 23 END."), "Found: a(ID). Expected: BEGIN at position 17"),
    ("no end", Left("PROGRAM _end; BEGIN a := 23 ."), "Found: .(DOT). Expected: END at position 29"),
    ("no semi colon between statements", Right("no_semi"), "Found: b(ID). Expected: END at position 81"),
    ("assignment op is invalid", Left("PROGRAM assign; BEGIN a = 25 END."), "Invalid token '=' at position 25"),
    ("no program token", Left("_program;"), "Found: _program(ID). Expected: PROGRAM at position 1"),
    ("no program name", Left("PROGRAM ; BEGIN END."), "Found: ;(SEMI). Expected: ID at position 9"),
    ("var without declaration", Left("PROGRAM _nodcl; VAR ;"), "Found: ;(SEMI). Expected: ID at position 21"),
    ("no colon", Left("PROGRAM _nocolon; VAR a INTEGER;"), "Found: INTEGER(INT_TYPE). Expected: : at position 25"),
    ("no type", Left("PROGRAM _notype; VAR a: ;"), "Found: ;(SEMI). Expected: INTEGER,REAL at position 25"),
    ("no comma", Left("PROGRAM _nocomma; VAR a b: REAL ;"), "Found: b(ID). Expected: : at position 25"),
    ("extra comma", Left("PROGRAM _extracomma; VAR a, , b: REAL;"), "Found: ,(COMMA). Expected: ID at position 29"),
    ("no semi colon after declaration", Left("PROGRAM _nosemi; VAR a: REAL BEGIN END."), "Found: BEGIN(BEGIN). Expected: ; at position 30"),
    ("no comments close", Left("PROGRAM _nocomments; VAR a: REAL; { BEGIN END."), "Found: (EOF). Expected: BEGIN at position 47")
  )

  private def run(text: String) = {
    Try {
      val node = Parser(Lexer(text)).expr
      Interpreter.visit(node, Map.empty)._1
    }.map(df.format)
      .recover { case e: IException =>
        e.message
      }.get
  }

  private def runProgram(program: Either[String, String]): String = {
    val text = program match {
      case Left(text) => text
      case Right(file) => Source.fromResource(s"programs/$file.pascal").mkString
    }
    Try(Parser(Lexer(text)).parse)
      .toEither match {
        case Left(e: IException) => e.message
        case Left(e: Throwable) => throw e
        case Right(_) => "Success"
      }
  }

  "Parser.expr" must {
    tests.foreach { case (testDesc, input, output) =>
      testDesc in {
        run(input) mustBe output
      }
    }
    "throw InvalidSyntaxException" when {
      syntaxTests.foreach { case (testDesc, input, errorMsg) =>
        testDesc in {
          run(input) mustBe errorMsg
        }
      }
    }
  }
  "Parser.parse" must {
    "throw error message" when {
      programErrorTests.foreach { case (testDesc, input, errorMsg) =>
        testDesc in {
          runProgram(input) mustBe errorMsg
        }
      }
    }
  }
}
