import business.Interpreter
import models.{AST, IException}
import org.scalatest.Inside
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.text.DecimalFormat
import scala.io.Source
import scala.language.postfixOps
import scala.util.Try

class InterpreterSpec extends AnyWordSpec with Matchers with Inside {
  private val df = new DecimalFormat("#")
  df.setMaximumFractionDigits(20)

  private val tests = Seq(
    ("parse pascal program",
      Right("program"),
      Map("number" -> 2, "a" -> 2, "b" -> 25, "c" -> 27, "x" -> 11, "d" -> 2.5)),
    ("parse empty program",
      Left("PROGRAM empty; BEGIN END."),
      Map.empty[String, Double]),
    ("parse program with lowercase keywords",
      Left("program lc; begin a:=25 EnD."),
      Map("a" -> 25)),
    ("parse program with case-insensitive variables",
      Right("ci"),
      Map("number" -> 2, "a" -> 2, "b" -> 25, "c" -> 27, "x" -> 11)),
    ("parse program with underscore variables",
      Left("program under; begin a:=25; _b:=a*2 EnD."),
      Map("a" -> 25, "_b" -> 50)),
    ("parse program with variable declarations",
      Right("var_program"),
      Map("number" -> 2, "a" -> 2, "b" -> 25, "c" -> 27, "x" -> 11, "y" -> 5.997142857142857)),
    ("parse program with comments",
      Right("comments"),
      Map("number" -> 2, "a" -> 2, "b" -> 25, "c" -> 27, "x" -> 11, "y" -> 5.997142857142857))
  )

  private val errorTests = Seq(
    ("variable is not defined", Left("PROGRAM undefined; BEGIN a:=x END."), "Undefined variable x at position 28")
  )

  private def run(program: Either[String, String]): Either[String, Map[String, Double]] = {
    val text = program match {
      case Left(text) => text
      case Right(file) => Source.fromResource(s"programs/$file.pascal").mkString
    }
    Try(Interpreter(text).interpret)
      .toEither
      .left.map {
        case e: IException =>
          e.message
        case e: Throwable => throw e
      }
      .map { case (result, memory) =>
        result mustBe "0"
        memory
      }
  }

  "Interpreter" must {
    tests.foreach { case (testDesc, input, output) =>
      testDesc in {
        inside(run(input)) {
          case Right(memory) =>
            memory.foreach { case (k, v) =>
              output.keySet must contain(k)
              output(k) mustBe v
            }
        }
      }
    }

    "throw error message" when {
      errorTests.foreach { case (testDesc, input, errorMsg) =>
        testDesc in {
          inside(run(input)) {
            case Left(e) => e mustBe errorMsg
          }
        }
      }
    }
  }
}
