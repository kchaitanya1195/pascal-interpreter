import business.Lexer
import models.Token
import models.TokenType._
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.annotation.tailrec

class LexerSpec extends AnyWordSpec with Matchers {
  private val tests = Seq(
    (
      "lex program",
      "BEGIN a := 2; END.",
      Seq(Token(BEGIN, "BEGIN", 0),
        Token(ID, "a", 6),
        Token(ASSIGN, ":=", 8),
        Token(INTEGER, "2", 11),
        Token(SEMI, ";", 12),
        Token(END, "END", 14),
        Token(DOT, ".", 17),
        Token(EOF, "", 18)
      )
    ),
    (
      "lex program without semicolon",
      "BEGIN a := 2 END.",
      Seq(Token(BEGIN, "BEGIN", 0),
        Token(ID, "a", 6),
        Token(ASSIGN, ":=", 8),
        Token(INTEGER, "2", 11),
        Token(END, "END", 13),
        Token(DOT, ".", 16),
        Token(EOF, "", 17)
      )
    ),
    (
      "lex program with expressions",
      "BEGIN a := 2 + 5 - 23; END.",
      Seq(Token(BEGIN, "BEGIN", 0),
        Token(ID, "a", 6),
        Token(ASSIGN, ":=", 8),
        Token(INTEGER, "2", 11),
        Token(PLUS, "+", 13),
        Token(INTEGER, "5", 15),
        Token(MINUS, "-", 17),
        Token(INTEGER, "23", 19),
        Token(SEMI, ";", 21),
        Token(END, "END", 23),
        Token(DOT, ".", 26),
        Token(EOF, "", 27)
      )
    ),
    (
      "lex program with real numbers",
      "5 + 2.5",
      Seq(Token(INTEGER, "5", 0),
        Token(PLUS, "+", 2),
        Token(REAL, "2.5", 4),
        Token(EOF, "", 7)
      )
    )
  )

  private def run(text: String) = {
    @tailrec def trRun(lexer: Lexer, tkList: Seq[Token]): Seq[Token] = {
      lexer.next() match {
        case tk if tk.tType == EOF => tkList :+ tk
        case tk => trRun(lexer, tkList :+ tk)
      }
    }

    trRun(Lexer(text), Seq.empty)
  }

  "Lexer" must {
    tests.foreach { case (desc, text, expTokens) =>
      desc in {
        run(text) must contain theSameElementsInOrderAs expTokens
      }
    }
    "peek next token type on head call" in {
      val lexer = Lexer("BEGIN a := 2; END.")

      lexer.next() mustBe Token(BEGIN, "BEGIN", 0)
      lexer.head mustBe ID
      lexer.next() mustBe Token(ID, "a", 6)
    }
  }
}
