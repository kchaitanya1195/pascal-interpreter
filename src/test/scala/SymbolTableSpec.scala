import business.SymbolTableBuilder
import models.Symbol
import models.Symbol.{BuiltInTypeSymbol, VarSymbol}
import models.{IException, SymbolTable}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.Inside

import scala.io.Source
import scala.util.Try

class SymbolTableSpec extends AnyWordSpec with Matchers with Inside {

  private def run(program: Either[String, String]): Either[String, SymbolTable] = {
    val text = program match {
      case Left(text) => text
      case Right(file) => Source.fromResource(s"programs/$file.pascal").mkString
    }
    Try(SymbolTableBuilder(text).build)
      .toEither
      .left.map {
        case e: IException =>
          e.message
        case e: Throwable => throw e
      }.map { result =>
        result
      }
  }

  private val builtInSymbols = SymbolTable().symbols
  private def varSymbol(name: String, sType: String): Symbol = new VarSymbol(name, builtInSymbols(sType))
  private val tests = Seq(
    ("parse empty program",
      Left("PROGRAM empty; BEGIN END."),
      builtInSymbols),
    ("parse variable program",
      Right("var_program"),
      builtInSymbols ++
        Map("number" -> varSymbol("number", "INTEGER"),
          "a" -> varSymbol("a", "INTEGER"),
          "b" -> varSymbol("b", "INTEGER"),
          "c" -> varSymbol("c", "INTEGER"),
          "x" -> varSymbol("x", "INTEGER"),
          "y" -> varSymbol("y", "REAL")))
  )
  private val errorTests = Seq(
    ("undefined type is used",
      Right("undefined_type"),
      "Found: STRING(ID). Expected: INTEGER,REAL at position 37"),
    ("undeclared variable is used",
      Right("undeclared_var"),
      "Undefined variable z at position 128")
  )

  "SymbolTable" must {
    tests.foreach { case (testDesc, input, expected) =>
      testDesc in {
        inside(run(input)) {
          case Right(table) =>
            table.symbols.foreach { case (k, v) =>
              expected.keySet must contain(k)
              expected(k) mustBe v
            }
        }
      }
    }
    "throw error" when {
      errorTests.foreach { case (testDesc, input, errorMsg) =>
        testDesc in {
          inside(run(input)) {
            case Left(e) => e mustBe errorMsg
          }
        }
      }
    }
  }
}
