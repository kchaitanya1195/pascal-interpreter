name := "pascal-interpreter"

version := "0.1"

scalaVersion := "3.1.1"

scalacOptions ++= Seq("--deprecation")

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.12" % Test
)
